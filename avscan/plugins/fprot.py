"""
Implementation of the AVAdapter for the FProt virus scanner
web site: http://www.f-prot.com/

Copyright (C) 2013  Lior Amar

This file is part of avscan module

avscan is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from avscan.av_adapter import IAVAdapter, ScannerThread, ScannerThreadStatusEnum
from avscan.core import AVInfo, AVResult, AVStatus, AvResultEnum

import re
import subprocess
from datetime import datetime
import time


class FProt(IAVAdapter):
    """
    Specific implementation of avadapter for clamav
    """
    my_name = "fprot"

    scan_summary_title = "Results:"

    def __init__(self):
        """
        Empty
        """
        self._line_parsed = 0
        self._cmd = "/opt/f-prot/fpscan"
        self._in_summary = False
        self._start_time = None
        self._end_time = None
        self._result = AVResult()
        self._verbose = False

    def verbose(self, v):
        self._verbose = v

    def info(self):
        info = AVInfo()
        info.name = self.my_name
        info.update_date = "Error"
        info.scanner_cmd = self._cmd

        # Getting the database age
        proc = subprocess.Popen([self._cmd, "--version"],
                                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        return_code = proc.wait()

        # Read from pipes
        #print("Ret code: {0}".format(return_code))
        if return_code != 0:
           info.update_date = "Error"
           info.db_age = 1000000
        else:
            for line in proc.stdout:
                l = line.decode()
                l = l.strip()
                l = l.lstrip()
                if l.startswith("Virus signatures:"):
                    #print("DDDD:[{0}]".format(l))

                    m = re.match(".*?(\d{4})(\d{2})(\d{2})", l)
                    if m:
                        info.update_date = "{0}-{1}-{2}".format(m.group(1), m.group(2), m.group(3))
                        fmt = '%Y-%m-%d'
                        d1 = datetime.strptime(info.update_date, fmt)
                        d2 = datetime.now()
                        info.db_age = (d2-d1).days
                        info.update_date = d1.strftime("%Y-%m-%d")

        return info

    def parse_summary_line(self, line):
        """
        Parsing a summary line of clamav
           Results:

            Files: 5603
            Skipped files: 0
            MBR/boot sectors checked: 0
            Objects scanned: 5604
            Infected objects: 0
            Infected files: 0
            Files with errors: 0
            Disinfected: 0

            Running time: 00:04

        """

        if len(line) == 0:
            return

        parts = re.split(":\s+", line)
        if self._verbose:
            print("Parts: {0}".format(str(parts)))
        data_parts = re.split("\s+", parts[1])
        if parts[0] == "Files":
            self._result.scanned_files = int(data_parts[0])
        elif parts[0] == "Infected objects":
            self._result.infected_files += int(data_parts[0])
        elif parts[0] == "Infected files":
            self._result.infected_files += int(data_parts[0])

    def parse_file_status_line(self, line):
        """
        Parsing line of the form: file-name: status-str
        """
        m = re.match("\[(.*)\]\s+\<(.*)\>\s+(.*)", line)
        if m:
            file_status = m.group(1)
            extra_info = m.group(2)
            filename = m.group(3)

        if file_status == "Error":
            self._result.error_list.append((filename, file_status + " " + extra_info))
            self._result.errors += 1
        else:
            self._result.infected_list.append((filename, file_status + " " + extra_info))

    def handle_output_line(self, line):
        self._line_parsed = self._line_parsed + 1

        line = line.strip()

        if self._verbose:
            print("fprot:{0} : {1} [{2}]".format(self._line_parsed, len(line), line))

        if not line or len(line) == 0:
            return True

        # 9 first lines contain info
        if (self._line_parsed >= 1 and self._line_parsed <= 9):
            return True
        if len(line) == 0:
            return True

        if line.startswith(self.scan_summary_title):
            self._in_summary = True
            return True
        elif self._in_summary:
            self.parse_summary_line(line)
        else:
            # Parsing a regular line: file: status
            self.parse_file_status_line(line)

        return True

    def scan(self, path):
        cmd = [self._cmd, "--report", "--nospin",  path]

        self.thread = ScannerThread(cmd=cmd, line_parser=self.handle_output_line)# pty=False, line_sep="\n")
        if self._verbose is True:
            self.thread.verbose = True

        self._start_time = time.time()
        self.thread.start()

    def status(self):
        s = AVStatus()
        thread_status = self.thread.get_status()
        s.status = ScannerThreadStatusEnum.thread_status_to_av_status(thread_status)
        s.msg = "parsed {0} lines".format(self._line_parsed)
        return s

    def result(self):
        """
        Return result object
        """

        result = self._result
        ts = self.thread.get_status()
        # Still running or failed we can not obtain results
        if ts is not ScannerThreadStatusEnum.ST_STATUS_DONE:
            result.status = None
        else:
            result.time = self.thread.end_time - self.thread.start_time
            if result.infected_files > 0:
                result.status = AvResultEnum.AV_RESULT_INFECTED
            elif result.errors > 0:
                result.status = AvResultEnum.AV_RESULT_ERROR
            else:
                result.status = AvResultEnum.AV_RESULT_OK

        return result

