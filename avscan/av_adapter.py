"""
AVAdapter base class for av scanners - or any check

o The object does not block, it should continue to run in the background

Copyright (C) 2013  Lior Amar

This file is part of avscan module

avscan is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import select
import pty
import os
import sys
import threading
import time
import subprocess


def fork_child_in_pty(cmd):
        """
        Run cmd in a pty so the program being run will behave as if running on
        a normal terminal
        """
        try:
            (child_pid, child_fd) = pty.fork()
        except OSError as e:
            print("fork failed {0}".format(e))
            return (None, None)

        # In child process
        if child_pid == pty.CHILD:
            try:
                os.execlp(cmd[0], *cmd)
            except:
                sys.stdout.write("CP_MONITOR_INTERNAL_ERROR: exec failed [{0}]".format(cmd[0]))
                sys.exit(2)

        # In parent process
        return (child_pid, child_fd)


def fork_child(cmd):
    """
    Use regular fork (without pty)
    """
    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
    return (p.pid, p.stdout.fileno())


class IAVAdapter():

    my_name = "none"
    @classmethod
    def av_adapter_for(cls, name):
        return name == cls.my_name

    def __init__(self):
        """
        A filler
        """

    def verbose(self, v):
        """
        Set verbose mode
        """

    def info(self):
        """
        Obtain an information object about this av
        """

    def init(self):
        """
        Initialize av if needed
        """

    def scan(self, path):
        """
        Scan the given path
        """

    def status(self):
        """
        Return status object of scan
        """

    def stop(self):
        """
        Stop scan (if running)
        """

    def results(self):
        """
        Return results object
        """

from avscan.core import AVStatusEnum

class ScannerThreadStatusEnum:
    ST_STATUS_NOT_STARTED = "not-started"
    ST_STATUS_RUNNING = "running"
    ST_STATUS_FAIL = "fail"
    ST_STATUS_DONE = "done"

    @classmethod
    def thread_status_to_av_status(cls, thread_status):
        if thread_status is ScannerThreadStatusEnum.ST_STATUS_NOT_STARTED:
            return AVStatusEnum.AV_STATUS_INIT
        elif thread_status is ScannerThreadStatusEnum.ST_STATUS_RUNNING:
            return AVStatusEnum.AV_STATUS_RUNNING
        elif thread_status is ScannerThreadStatusEnum.ST_STATUS_DONE:
            return AVStatusEnum.AV_STATUS_DONE
        elif thread_status is ScannerThreadStatusEnum.ST_STATUS_FAIL:
            return AVStatusEnum.AV_STATUS_FAIL


class ScannerThread(threading.Thread):
    """
    A thread used to run an external command and read its output line by line
    This thread will make sure all line are read and will combine partial reads.
    """
    def __init__(self, cmd, line_parser, pty=True, line_sep="\r\n"):
        threading.Thread.__init__(self)
        self.timeout = 0.5
        self.verbose = False
        self.cmd = cmd
        self.line_parser = line_parser
        self.in_pty = pty
        self.line_sep = line_sep

        self.child_pid = -1
        self.child_exit_status = -1

        self.master_fd = None
        self.line_number = 0
        self.partial_line = ""
        self.partial_read_count = 0
        self.current_count_error = 0
        self.start_time = None
        self.end_time = None
        self.status = ScannerThreadStatusEnum.ST_STATUS_NOT_STARTED
        self.error_list = []  # The list will contain tuples of file name and error

    def get_status(self):
        return self.status

    def run_old(self):
        # Running in a different thread
        print("Running: {0}".format(str(self.cmd)))


        #if self.verbose :
        #    print "Running command {0}".format(cmd)
        if self.in_pty:
            (child_pid, child_fd) = fork_child_in_pty(self.cmd)
        else:
            (child_pid, child_fd) = fork_child(self.cmd)

        if ((not child_pid) or (not child_fd)):
            print("Error: trying to spawn child process")
            return None

        # Saving the pid of child process
        self.child_pid = child_pid

        self.line_number = 0
        line_list = []
        while True:
            #print "Lines", line_list
            if len(line_list) == 0:
                time.sleep(0.01)
                rlist, wlist, elist = select.select([child_fd], [], [], self.timeout)
                if rlist:
                    line_list = self.read_lines_from_child(child_fd)
                    if not line_list:
                        break
                else:
                    if self.verbose:
                        print("Timeout")
            else:
                res = self.line_parser(line_list.pop(0))
                if not res:
                    return False
        print("thread: out of loop")

    def run(self):
        # Running in a different thread
        if self.verbose:
            print("Running: {0}".format(str(self.cmd)))


        #if self.verbose :
        #    print "Running command {0}".format(cmd)
        self.start_time = time.time()
        if self.in_pty:
            (child_pid, child_fd) = fork_child_in_pty(self.cmd)
        else:
            (child_pid, child_fd) = fork_child(self.cmd)

        if ((not child_pid) or (not child_fd)):
            self.status = ScannerThreadStatusEnum.ST_STATUS_FAIL
            print("Error: trying to spawn child process")
            return None

        self.status = ScannerThreadStatusEnum.ST_STATUS_RUNNING
        # Saving the pid of child process
        self.child_pid = child_pid
        self.line_number = 0
        child_stdout = os.fdopen(child_fd)

        while True:
            try:
                line = child_stdout.readline()
                if line:
                    res = self.line_parser(line)
                    if not res:
                        self.end_time = time.time()
                        return False
                else:
                    break
            except OSError:
                break

        # Check if child is alive
        (pid, status) = os.waitpid(self.child_pid, os.WNOHANG)
        self.status = ScannerThreadStatusEnum.ST_STATUS_DONE
        if os.WIFEXITED(status):
            self.child_exit_status = os.WEXITSTATUS(status)
            if self.verbose:
                print("Child exited using exit(): {0}".format(self.child_exit_status))

        self.end_time = time.time()
        if self.verbose:
            print("thread: out of loop")


    def read_lines_from_child(self, child_fd):
        """
        Read lines from child_fd. Return list of lines

        The method read as much as possible from the fd. If the buffer
        read does not end with a newline then the last line (which is
        partial) is saved for future read.
        """

        try:
            size = 65536
            buff = os.read(child_fd, size)

            if self.verbose:
                print("Buff:[", buff, "]********")

            buff = buff.decode()
            if self.verbose:
                print("Buff: len = {0} [".format(len(buff)), buff, "]++++++")

            buff_len = len(buff)
            if buff_len == 0:
                (pid, status) = os.waitpid(self.child_pid, os.WNOHANG)
                if os.WIFEXITED(status):
                    self.child_done = True
                    self.child_exit_status = os.WEXITSTATUS(status)
                    if self.verbose:
                        print("Child exited using exit(): {0}".format(self.child_exit_status))
                return None

            # A special case where the previous partial line was read without the \r\n
            if self.line_sep == "\r\n":
                if(len(buff) >= 2 and buff[0] == "\r" and buff[1] == "\n" ):
                    if(self.partial_line):
                        self.partial_line += "\r\n"

            buff = buff.lstrip()


            half_read = False
            if self.verbose:
                print("\n\n\nBufflen: ", buff_len)

            #print("Testing for new line at end of buffer buff_len = {0} size = {1}".format(buff_len, size))
#            if (buff_len == size - 1) and (buff[-1] != '\n'):
            if (buff[-1] != '\n') and (buff[-1] != '\r'):
                if self.verbose:
                    print("Detected possible problem in buffer")
                self.partial_read_count += 1
                half_read = True

            # In case of a previously partial line we preappend it to buff
            if self.partial_line:
                if self.verbose:
                    print("Prepending partial line [{0}]".format(self.partial_line))
                buff = self.partial_line + buff
                self.partial_line = ""

            tmp_list = buff.split(self.line_sep)

            if tmp_list[-1] == "":
                if self.verbose:
                    print("Removing empty part at end of list")
                tmp_list.pop()

            if self.verbose:
                print("Number of lines: {0}\n".format(len(tmp_list)))

            if half_read:
                self.partial_line = tmp_list.pop()
                if self.verbose:
                    print("Partial [{0}]   |".format(self.partial_line))

            if self.verbose:
                print("\n\nGot {0} lines size {1}\n\n".format(len(tmp_list), len(buff)))

            #self.line_list.pop()
            return tmp_list
            #line = child_f.readlines()
            #print "LLL", line

        except OSError as e:
            if self.verbose :
                print("Got exception on child_fd probably copy is done")
            (pid, status) = os.waitpid(self.child_pid, os.WNOHANG)
            if os.WIFEXITED(status):
                self.child_done = True
                self.child_exit_status = os.WEXITSTATUS(status)
                if self.verbose:
                    print("Child exited using exit() {0}".format(self.child_exit_status))
            return None
