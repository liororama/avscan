"""
Implementation of the AVAdapter for the AVG virus scanner
web site: http://free.avg.com

Copyright (C) 2013  Lior Amar

This file is part of avscan module

avscan is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from avscan.av_adapter import IAVAdapter, ScannerThread, ScannerThreadStatusEnum
from avscan.core import AVInfo, AVResult, AVStatus, AvResultEnum

import sys
import os
import re
import subprocess
from datetime import datetime
import time


class AVG(IAVAdapter):
    """
    Specific implementation of avadapter for avg
    """
    my_name = "avg"

    scan_summary_title = "Files scanned "

    def __init__(self):
        """
        Empty
        """
        self._line_parsed = 0
        self._cmd = "/usr/bin/avgscan"
        self._in_summary = False
        self._start_time = None
        self._end_time = None
        self._result = AVResult()
        self._verbose = False

    def verbose(self, v):
        self._verbose = v

    def info(self):
        info = AVInfo()
        info.name = self.my_name
        info.update_date = "Error"
        info.scanner_cmd = self._cmd

        # Getting the database age
        non_existing_file = "/non_existing_12345"
        while os.path.exists(non_existing_file):
            non_existing_file += "_1"

        proc = subprocess.Popen([self._cmd,  non_existing_file],
                                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        return_code = proc.wait()

        # Read from pipes
        #print("Ret code: {0}".format(return_code))
        for line in proc.stdout:
            l = line.decode()
            l = l.strip()
            l = l.lstrip()
            if l.startswith("Virus database release date:"):
                # example of date: Virus database release date: Sun, 22 Dec 2013 18:44:00 +0200
                m = re.match(".*?:\s+.*?,\s+(.*)\s+\d+:\d+:\d+", l)
                if m:
                    fmt = '%d %b %Y'
                    d1 = datetime.strptime(m.group(1).lstrip(), fmt)
                    d2 = datetime.now()
                    info.db_age = (d2-d1).days
                    info.update_date = d1.strftime("%Y-%m-%d")

        return info

    def parse_summary_line(self, line):
        """
        Parsing a summary lines of avg
            Files scanned     :  6764(6764)
            Infections found  :  0(0)
            PUPs found        :  0
            Files healed      :  0
            Warnings reported :  0
            Errors reported   :  20

        """

        if len(line) == 0:
            return

        parts = re.split(":\s+", line)
        if self._verbose:
            print("Parts: {0}".format(str(parts)))
        if parts[0].startswith("Files scanned"):
            m = re.match("(\d+)\(\d+\)", parts[1])
            if m:
                self._result.scanned_files = int(m.group(1))
            return
        elif parts[0].startswith("Infections found"):
            if self._verbose:
                print("found infected files")
            m = re.match("(\d+)\(\d+\)", parts[1].strip())
            if m:
                self._result.infected_files += int(m.group(1))
        elif parts[0].startswith("Errors reported"):
            self._result.errors += int(parts[1].strip())

    def parse_file_status_line(self, line):
        """
        Parsing line of the form: file-name: status-str
        """
        #if self._verbose:
        #    print("LLLLLL [{0}]".format(line))
        m = re.match("^(.*)\s+(Virus identified\s+.*)", line)
        if m:
            filename = m.group(1)
            virus_type = m.group(2)
            self._result.infected_list.append((filename,  virus_type))
            return

        m = re.match("^(.*?)\s+(Object\s+scan\s+failed\;\s+.*)", line)
        if m:
            filename = m.group(1)
            error_type = m.group(2)
            self._result.error_list.append((filename, error_type))
            return

        else:
            #if self._verbose:
            #sys.stderr.write("==============unidentified line [{0}]\n\n".format(len(line)))
            #for c in line:
            #    print("AAAAA [{0} | {1} ]".format(c, ascii(c)))
            m = re.match(".*\x1b.*", line)
            if(m):
                #print("got line with backslash R")
                return
            self._result.infected_list.append(("[" + line + "]", "unidentified status line (considered as infected)"))
            self._result.infected_files += 1

    def handle_output_line(self, line):
        self._line_parsed = self._line_parsed + 1

        line = line.strip()

        if self._verbose:
            print("avg:{0} : {1} [{2}]".format(self._line_parsed, len(line), line))

        if not line or len(line) == 0:
            return True

        # 5 first lines contain info
        if (self._line_parsed >= 1 and self._line_parsed <= 5):
            return True

        # Skipp empty lines
        if len(line) == 0:
            return True

        if line.startswith("\x1b"):
            return True

        if line.startswith(self.scan_summary_title):
            self._in_summary = True
            self.parse_summary_line(line)
            return True
        elif self._in_summary:
            self.parse_summary_line(line)
        else:
            # Parsing a regular line: file: status
            self.parse_file_status_line(line)

        return True

    def scan(self, path):
        cmd = [self._cmd, "--report", "/tmp/avg" ,      path]

        self.thread = ScannerThread(cmd=cmd, line_parser=self.handle_output_line, pty=False, line_sep="\n")
        if self._verbose is True:
            self.thread.verbose = True

        self._start_time = time.time()
        self.thread.start()

    def status(self):
        s = AVStatus()
        thread_status = self.thread.get_status()
        s.status = ScannerThreadStatusEnum.thread_status_to_av_status(thread_status)
        s.msg = "parsed {0} lines".format(self._line_parsed)
        return s

    def result(self):
        """
        Return result object
        """

        result = self._result
        ts = self.thread.get_status()
        # Still running or failed we can not obtain results
        if ts is not ScannerThreadStatusEnum.ST_STATUS_DONE:
            result.status = None
        else:
            result.time = self.thread.end_time - self.thread.start_time
            if result.infected_files > 0:
                result.status = AvResultEnum.AV_RESULT_INFECTED
            elif result.errors > 0:
                result.status = AvResultEnum.AV_RESULT_ERROR
            else:
                result.status = AvResultEnum.AV_RESULT_OK

        return result

