#!env python

import sys
import time
print("line1")
print("line2")

prefix = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
for i in range(1000):
    sys.stdout.write("\r {0} / ".format(prefix))
    sys.stdout.flush()
    time.sleep(0.05)

    sys.stdout.write("\r {0} - ".format(prefix))
    sys.stdout.flush()
    time.sleep(0.05)

    sys.stdout.write("\r {0} \ ".format(prefix))
    sys.stdout.flush()
    time.sleep(0.05)

    sys.stdout.write("\r {0} | ".format(prefix))
    sys.stdout.flush()
    time.sleep(0.05)

print("Line 4")
print("Line 5")
