
// Loading the clamav engine and getting the update time of the virus
// db. This time is not currently provided via the clamscan cli
// 
// On success exit with 0 and print YYYY-MM-DD,SEC-DIFF-FROM-NOW
// On error exit(1)
// SEC-DIFF-FROM-NOW is the number of seconds from the update until now
// May be negative !!!!
// 
// Compiling:
//   gcc -g clamav_update_date.c  -o ttt -I /opt/clamav/include/ -L /opt/clamav/lib/ -lclamav
//
// Running:
//   env LD_LIBRARY_PATH=/opt/clamav/lib  ./ttt 2>/dev/null
//
//  Redirecting 2>/dev/null since libclam av prints warnings in case the db is older than
//  7 days.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <clamav.h>
#include <time.h>



int main(int argc, char *argv[])
{
  time_t t;
  time_t now;
  struct tm *tmp;
  char outstr[200];

  if (1 > argc) {
    printf("usage:\n\t%s\n");
    exit(1);
  }
  
  if (CL_SUCCESS == cl_init(CL_INIT_DEFAULT)) {
    struct cl_engine *engine = cl_engine_new();
    const char *dbDir = "/opt/clamav/var/lib/clamav/"; //cl_retdbdir();
    int signatureNum = 0;
    int err;

    //printf("use default db dir '%s'\n", dbDir);
    if (CL_SUCCESS == cl_load(dbDir, engine, &signatureNum, CL_DB_STDOPT) &&
	CL_SUCCESS == cl_engine_compile(engine)) {
      
      // Getting the time parameter from the engine
      t = cl_engine_get_num(engine, CL_ENGINE_DB_TIME, &err);
      
      //printf("dbtime: %s\n", ctime(&t));
      tmp = localtime(&t);
      if (tmp == NULL) {
	perror("localtime");
	exit(1);
      }
      
      if (strftime(outstr, sizeof(outstr), "%Y-%m-%d" , tmp) == 0) {
	fprintf(stderr, "strftime returned 0");
	exit(EXIT_FAILURE);
      }
      now = time(NULL);
      printf("%s,%ld\n", outstr, now - t);
      
    }
    else {
      printf("failed to load databases!\n");
      exit(1);
    }
    cl_engine_free(engine);
  }
  return 0;
  
}
